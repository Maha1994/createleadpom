package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import week5.Day1.ProjectMethods;

public class CreateLeadPage extends ProjectMethods {
	public CreateLeadPage() {
		PageFactory.initElements(driver, this);
		}
	
	@FindBy(how=How.ID,using="createLeadForm_companyName")
	WebElement elecompname;
	public CreateLeadPage typecompname(String data) {
		type(elecompname,data);
		return this;
	}
	@FindBy(how=How.ID,using="createLeadForm_firstName")
	WebElement elefirstname;
	public CreateLeadPage typefirstname(String data) {
		type(elefirstname,data);
		return this;
	}
	@FindBy(how=How.ID,using="createLeadForm_lastName")
	WebElement elelastname;
	public CreateLeadPage typelastname(String data) {
		type(elelastname,data);
		return this;
	}
	@FindBy(how=How.CLASS_NAME,using="smallSubmit")
	WebElement esubmit;
	public ViewLeadPage clicklead() {
		click(esubmit);
		return new ViewLeadPage();
	}
	

}
