package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import week5.Day1.ProjectMethods;

public class HomePage extends ProjectMethods {
	
	public HomePage() {
		PageFactory.initElements(driver, this);
	}
	
	
@FindBy(how=How.LINK_TEXT,using="CRM/SFA")
	WebElement ecrmsfa;
	public MyHomePage clickcrmsfa() {
		click(ecrmsfa);
		return new MyHomePage();
	}

}
