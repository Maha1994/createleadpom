package testcase_usingPOM;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import week5.Day1.ProjectMethods;


public class Testcase_CreateLead extends ProjectMethods {
	@BeforeTest
	public void setData() {
		testCaseName = "CreateLead";
		testCaseDesc="Create a new Lead";
		category="Sanity";
		author="DB & Maha";
		
	}
	
	@Test (dataProvider="fetchdata")
	public void CreateLead(String uname,String password,String cname,String fname,String lname) {
		new LoginPage()
		.typeusername(uname)
		.typepassword(password)
		.clicklogin()
		.clickcrmsfa()
		.clickcreateLead()
		.typecompname(cname)
		.typefirstname(fname)
		.typelastname(lname)
		.clicklead()
		.verifycname(cname);
	}

	
		


	}

