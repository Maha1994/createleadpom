package week4.Day2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import week5.Day1.ProjectMethods;

public class TC002_CreateLead extends ProjectMethods{
	
	@BeforeTest
	public void setData() {
		testCaseName = "TC002_CreateLead";
		testCaseDesc="Create a new Lead";
		category="Sanity";
		author="DB & Maha";
		
		
		
		
	}
			
			@Test
			public void createLead() {
				WebElement createlead = locateElement("linkText","Create Lead");
				click(createlead);
				WebElement companyname = locateElement("id","createLeadForm_companyName");
				type(companyname, "DB Constructions");
				WebElement firstname = locateElement("id","createLeadForm_firstName");
				type(firstname, "Dillibabu");
				WebElement lastname = locateElement("id","createLeadForm_lastName");
				type(lastname, "Ruthiran");
				WebElement createlead1 = locateElement("class", "smallSubmit");
				click(createlead1);
				
			
		}

		}
		// TODO Auto-generated method stub

	
