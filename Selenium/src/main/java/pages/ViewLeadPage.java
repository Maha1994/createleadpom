package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import week5.Day1.ProjectMethods;

public class ViewLeadPage extends ProjectMethods {
	public ViewLeadPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.ID,using="viewLead_companyName_sp")
	WebElement elecname;
	public ViewLeadPage verifycname(String data) {
		verifyPartialText(elecname, data);
		return this;
	}
	@FindBy(how=How.ID,using="viewLead_firstName_sp")
	WebElement elefname;
	public ViewLeadPage verifyfname(String data) {
		verifyPartialText(elefname, data);
		return this;
	}
	@FindBy(how=How.ID,using="viewLead_lastName_sp")
	WebElement elelname;
	public ViewLeadPage verifylname(String data) {
		verifyPartialText(elelname, data);
		return this;
	}

}
