package week5.Day1;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnHTMLReports {

	public static void main(String[] args) throws IOException {
		ExtentHtmlReporter html=new ExtentHtmlReporter("./report/result.html");
		html.setAppendExisting(true);
		ExtentReports extent=new ExtentReports();
		extent.attachReporter(html);
		ExtentTest test = extent.createTest("TC001_CreateLead", "Create a new lead in leaftaps");
		test.assignCategory("Smoke");
		test.assignAuthor("DB and Maha");
		test.pass("Browser launched successfully",MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		
		extent.flush();
		
		

	}

}
