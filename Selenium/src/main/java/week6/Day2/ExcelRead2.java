package week6.Day2;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import week5.Day1.ProjectMethods;

public  class ExcelRead2 {
	public static Object[][] getExceldata() throws IOException {
		XSSFWorkbook wbook=new XSSFWorkbook("./Data/CreateLead.xlsx");
		XSSFSheet sheet = wbook.getSheetAt(0);
		int rowCount = sheet.getLastRowNum();
		System.out.println("Row count = "+rowCount);
		int columnCount = sheet.getRow(0).getLastCellNum();
		System.out.println("Column count = "+columnCount);
		
		Object[][] data = new Object[rowCount][columnCount];
		
		
		for(int j=1;j<=rowCount;j++) {
			XSSFRow row = sheet.getRow(j);
			for (int i = 0; i < columnCount; i++) {
				XSSFCell cell = row.getCell(i);
				String cellValue=cell.getStringCellValue();
				data[j-1][i] = cell.getStringCellValue();
				System.out.println(cellValue);
					
				}
			}
	
		return data;
	
		}
		
				
				
	
		
	}

