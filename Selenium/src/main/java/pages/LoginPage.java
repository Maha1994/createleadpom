package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import week5.Day1.ProjectMethods;

public class LoginPage extends ProjectMethods {

	public LoginPage() {
		PageFactory.initElements(driver, this);
	}
	
		@FindBy(how=How.ID,using="username")
		WebElement eleusername;
		public LoginPage typeusername(String data) {
			type(eleusername,data);
			return this;
		}
		
		@FindBy(how=How.ID,using="password")
		WebElement elepassword;
		public LoginPage typepassword(String data) {
			type(elepassword,data);
			return this;
		}
		@FindBy(how=How.CLASS_NAME,using="decorativeSubmit")
		WebElement elogin;
		public HomePage clicklogin() {
			click(elogin);
			return new HomePage();
		}
		}
		
		
		
		
		
		

	


